#!/bin/bash

# Copyright (c) 2020 Spiideo AB

# ARG_HELP([List groups via api-service])
# ARG_OPTIONAL_SINGLE([debug], ,[Turn on debug mode], [false])
# ARG_OPTIONAL_SINGLE([environment], [e], [Environment ('dev', 'prod' or 'localhost')], [prod])
# ARG_OPTIONAL_SINGLE([scope], [s], [Scope ('app' or 'control')], [control])
# ARG_TYPE_GROUP_SET([scopes], [SCOPE], [scope], [app,cs,control])
# ARG_TYPE_GROUP_SET([environments], [ENVIRONMENT], [environment], [dev,prod,localhost])
# ARG_POSITIONAL_SINGLE([from], [Date to list from])
# ARG_POSITIONAL_SINGLE([to], [Date to list to])
# ARG_POSITIONAL_SINGLE([account_id],  [The id of the account to copy reports for], [400728b4-37e7-4138-98aa-355850518b6a])
# ARGBASH_GO

eval $(spd_argbash_parse)
source /Users/jonas/repos/tools/jonas-spiideo/spd_common

# Get games for account
games=$(spd_apiservice_games_list --debug $_arg_debug -e $_arg_environment --scope $_arg_scope --state finished  --account_id $_arg_account_id --page_size 200 --from $_arg_from --to $_arg_to)
account_name=$(spd_apiservice_account_get --debug $_arg_debug -e $_arg_environment --scope $_arg_scope  $_arg_account_id | jp name)
nbr_games=$(echo $games | jp 'length(content)')

echo "Copy reports for account $(echo $account_name). Working with $nbr_games Spiideo-games"

game_ids=$(echo $games | jp 'content[*].id' | json -a)

# Iterate ove games
for game_id in $game_ids; do
    # Fetch Spiideo game
    game=$(spd_apiservice_game_get  --debug $_arg_debug -e $_arg_environment --scope $_arg_scope $game_id)
    game_start_time="$(echo "$game" | jp startTime)"
    game_title="$(echo "$game" | jp title)"
    scene_id=$(echo $game | jp sceneId | tr -d '"')

    # Match games with a start_time withing two hours (?!) from Spiideo's
    two_hours_micros=$((2*60*60*1000000))
    from=$(($game_start_time - $two_hours_micros))
    to=$(($game_start_time + $two_hours_micros))

    # Fetch scene and get the external scene-ref and resolve it to venue (Signality concept of scene)
    scene_ref=$(spd_apiservice_scene_get --debug $_arg_debug -e $_arg_environment --scope $_arg_scope $scene_id | jp 'externalSceneRef'  | tr -d '"')
    venue_id=$(spd_sideorderservice_venue_mapping_get --debug $_arg_debug -e $_arg_environment $scene_ref | jp venueId | tr -d '"')

    # Fetch Signality game list on the venue
    signality_games=$(spd_signality_games_list --debug $_arg_debug $venue_id)
    id_and_start_times=$(echo $signality_games | jp '[].{id:id,time_start:time_start}' | jq -r '.[] | [.id, .time_start] | @csv' )
    matching_signality_game_id=""
    cur_diff=1000000000

    echo "Working with Spiideo game $game_id $game_title matching $(echo $signality_games | jp 'length(@)') Signality-games on $scene_ref."

    # Iterate over signality games and look for the best match
    for id_and_start_time in $id_and_start_times; do
        id=$(echo $id_and_start_time  | cut -f1 -d ',' | tr -d '"')
        start_time=$(echo $id_and_start_time  | cut -f2 -d ',' | tr -d '"' | xargs spd_time_parse_to_epoch)

        # Check if game is a potential match
        if [ $start_time -gt $from ] && [ $start_time -lt $to ]; then
            diff=$(($game_start_time - $start_time))
            abs_diff=$(echo $diff | tr -d '-')

            # Check if this is a closer match than the current
            if [ "$cur_diff" -gt "$abs_diff" ]; then
                # We have a new candidate
                cur_diff="$abs_diff"
                matching_signality_game_id="$id"
            fi
        fi

    done

    if [ ! -z "$matching_signality_game_id" ]; then
        # Download the PDF report
        if [ -f /tmp/report.pdf ]; then
           rm /tmp/report.pdf
        fi
        echo "Closest Signality game is $matching_signality_game_id, downloading report..."
        last_phase_id=$(spd_signality_phases_get --debug $_arg_debug $matching_signality_game_id | jp 'max_by([], &number).id' | tr -d '"')
        spd_signality_report_get --debug $_arg_debug -e $_arg_environment  $matching_signality_game_id $last_phase_id > /tmp/report.pdf

        # Upload the report to S3 game-data-bucket
        echo "Uploading report..."
        upload_url=$(spd_contentservice_signality_report_url_for_game --debug $_arg_debug -e $_arg_environment  $game_id | tr -d '"')
        http --quiet put "$upload_url" Content-Type:application/pdf < /tmp/report.pdf

        echo "Done"
    fi

done