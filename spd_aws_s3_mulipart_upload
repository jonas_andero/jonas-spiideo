#!/bin/bash

# Copyright (c) 2020 Spiideo AB

# ARG_HELP([Upload a file to S3 using multi-part])
# ARG_OPTIONAL_SINGLE([debug], ,[Turn on debug mode], [false])
# ARG_POSITIONAL_SINGLE([profile_name], [Name of the AWS-credentials profile to get credentials from])
# ARG_POSITIONAL_SINGLE([file_path], [Path to the file to upload])
# ARG_POSITIONAL_SINGLE([bucket], [Name of the bucket to upload to])
# ARG_POSITIONAL_SINGLE([key], [The key of the S3 item to create])
# ARG_POSITIONAL_SINGLE([size], [The size of the files to split the source in bytes (Default 5Mb)], [5000000])
# ARGBASH_GO

eval $(spd_argbash_parse)
source /Users/jonas/repos/tools/jonas-spiideo/spd_common

# Split the input into temporary files in a temp-dir
temp_dir=$(mktemp -d)
split -b $_arg_size  "$_arg_file_path" "$temp_dir/part."
file_parts=$(ls "$temp_dir/part."*)
nbr_parts=$(ls -l $temp_dir | wc -l)

# Initiate upload
upload=$(aws --profile "$_arg_profile_name" s3api create-multipart-upload --bucket "$_arg_bucket" --key "$_arg_key")
upload_id=$(echo "$upload" | jp 'UploadId' | json -a)

# Upload each part and cleanup temp files
index=1
upload_parts=$(
    for file_part in $file_parts; do
        upload_part=$(aws --profile "$_arg_profile_name" s3api upload-part --bucket "$_arg_bucket" --key "$_arg_key" --upload-id "$upload_id" --part-number "$index" --body "$file_part")
        echo "$upload_part" | json -e "this.PartNumber=$index"
        index=$(($index + 1))
        rm $file_part
    done | json -g
)
rmdir "$temp_dir"

# Finalize uplad
multipart_json_path=$(mktemp)
jo Parts="$upload_parts" > "$multipart_json_path"
aws --profile "$_arg_profile_name" s3api complete-multipart-upload --multipart-upload "file://$multipart_json_path" --bucket "$_arg_bucket" --key "$_arg_key" --upload-id "$upload_id"

rm "$multipart_json_path"
