#!/bin/bash

# Copyright (c) 2019 Spiideo AB
function print_databases() {
	lpass ls | grep "@ Aurora" | cut -f2 -d ' ' | cut -f2 -d '/' | sort -u | tr '[:upper:]' '[:lower:]'
}

# ARG_HELP([Get postgress credentials from lastpass])
# ARG_OPTIONAL_SINGLE([debug], ,[Turn on debug mode], [false])
# ARG_POSITIONAL_SINGLE([env], [The environement to target])
# ARG_POSITIONAL_SINGLE([database], [The name of the database])
# ARG_TYPE_GROUP_SET([environment], [Environment], [env], [dev,prod])
# ARGBASH_GO
eval $(spd_argbash_parse)
source /Users/jonas/repos/tools/jonas-spiideo/spd_common

databases=$(print_databases)
if [[ -z "$(echo $databases | grep $_arg_database)" ]]; then
	print_help
	echo ""
	echo "Error: $_arg_database is not found among the database credentials in lastpass. Possible values are:"
	echo ""
	echo "$databases"
	exit 2
fi
# Parse arguments
env_lower="$(tr '[:upper:]' '[:lower:]' <<< $_arg_env)"
env_camel="$(tr '[:lower:]' '[:upper:]' <<< ${env_lower:0:1})${env_lower:1}"
service_lower="$(tr '[:upper:]' '[:lower:]' <<< $_arg_database)"
service_camel="$(tr '[:lower:]' '[:upper:]' <<< ${service_lower:0:1})${service_lower:1}"

# Get database details from lastpass
note=$(lpass show "$service_camel @ Aurora $env_camel")
username=$(echo "$note" | grep "Username:" | cut -f2 -d ' ')
password=$(echo "$note" | grep "Password:" | cut -f2 -d ' ')
database=$(echo "$note" | grep "Database:" | cut -f2 -d ' ')
port=$(echo "$note" | grep "Port:" | cut -f2 -d ' ')
hostname=$(echo "$note" | grep "Hostname:" | cut -f2 -d ' ')
password_urlencoded=$(echo $password | php -R 'echo urlencode($argn);')
echo '{"username": "'$username'", "password": "'$password'", "database": "'$database'", "port": "'$port'", "hostname": "'$hostname'", "password_urlencoded": "'$password_urlencoded'"}'